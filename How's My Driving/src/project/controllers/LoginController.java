package project.controllers;

import com.sun.javaws.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import project.MainClass;
import project.utility.Security;
import project.utility.XML;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

/**
 * Created by Justin on 12-8-2016.
 */
public class LoginController implements Initializable {

    //Injected Variables
    @FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Button btnLogin;
    @FXML
    private Button btnSignup;
    @FXML
    private Hyperlink hypForgotPass;

    //Variables
    private StringProperty Username = new SimpleStringProperty();
    private StringProperty Password = new SimpleStringProperty();

    private String IP;
    private String Instance;
    private String Database;
    private String Port;
    private String ConnString;

    // actual connection to db
    private Connection Conn = null;
    // object used to issue SQL commands
    private Statement stmt = null;

    public void btnLoginOnClick() {
        try {
            ValidateEmail();
            String salt = RetrieveSalt();
            ValidatePassword(salt);

            //Display Main Menu
            Parent root = FXMLLoader.load(MainClass.class.getResource("views/UserMainMenu.fxml"));

            Stage stage = new Stage();
            stage.setTitle("My New Stage Title");

            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getVisualBounds();

            stage.setScene(new Scene(root, bounds.getWidth(), bounds.getHeight()));
            stage.show();

            MainController.PrimaryStage.hide();
            MainController.PrimaryStage = stage;
        }
        catch (Exception e) {
            project.utility.General.DisplayWarning("Oops!", e.getMessage());
        }
    }

    private void getDBConn() {
        try {
            Element DBVars = XML.loadDoc();

            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            } catch (Exception e) {
                throw new Exception("Error loading necessary network drivers");
            }

            //Get IP
            NodeList IPSetting = DBVars.getElementsByTagName("IP");
            IP =  IPSetting.item(0).getTextContent();

            //Get Instance
            NodeList InstanceSetting = DBVars.getElementsByTagName("Instance");
            Instance = InstanceSetting.item(0).getTextContent();

            //Get Instance
            NodeList PortSetting = DBVars.getElementsByTagName("Port");
            Port = PortSetting.item(0).getTextContent();

            //Get Database
            NodeList DBSetting = DBVars.getElementsByTagName("Database");
            Database = DBSetting.item(0).getTextContent();

            ConnString = String.format("jdbc:sqlserver://%1s\\%2s:%3s;databaseName=%4s", IP, Instance, Port, Database);
            Conn = DriverManager.getConnection(ConnString, MainController.DBUser, MainController.DBPass);
            stmt = Conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        }
        catch (Exception e) {
            project.utility.General.DisplayWarning("Oops!", "Error establishing connection to server, please try again later.");
        }
    }

    private void ValidateEmail() throws Exception{
        String sql = String.format("SELECT COUNT(*) FROM dbo.Users WHERE Email = '%1s'", Username.get());
        ResultSet result;
        try {
            result = stmt.executeQuery(sql);
            int count = -1;
            while (result.next()) {
                count = Integer.parseInt(result.getString(1));
            }
            if (count == -1)
                throw new Exception("Connection error, please try again later");
            if (count == 0)
                throw new Exception("Email address does not exist");
        }
        catch (Exception e) {
            throw new Exception("Cannot retrieve email info");
        }
    }

    private String RetrieveSalt() throws Exception{
        String sql = String.format("SELECT Salt FROM dbo.Users WHERE Email = '%1s'", Username.get());
        ResultSet result;
        try {
            result = stmt.executeQuery(sql);
            String salt = "null";
            while (result.next()) {
                salt = result.getString("salt");
            }
            if (salt.equals("null"))
                throw new Exception("Error occurred receiving salt, please try again");
            return salt;
        }
        catch (Exception e) {
            throw new Exception("Error occurred receiving salt, please try again");
        }
    }

    private void ValidatePassword(String Salt) throws Exception {
        String Composite = Password.get().concat(Salt);
        String Hash = Security.MD5HashString(Composite);
        String sql = String.format("SELECT COUNT(*) FROM dbo.Users WHERE Email = '%1s' AND Hash = '%2s'", Username.get(), Hash);
        ResultSet result;
        try {
            result = stmt.executeQuery(sql);
            int count = -1;
            while (result.next()) {
                count = Integer.parseInt(result.getString(1));
            }
            if (count == -1)
                throw new Exception("Connection error, please try again later");
            if (count == 0)
                throw new Exception("Password Incorrect");
        }
        catch (Exception e) {
            throw new Exception("Error occurred validating password, please try again");
        }

    }

    public void btnSignupOnClick() {
    }

    public void hypForgotPassOnClick() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtUsername.textProperty().bindBidirectional(Username);
        txtPassword.textProperty().bindBidirectional(Password);
        getDBConn();
    }


}
