package project;

/**
 * Created by Justin on 12-8-2016.
 */
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import project.controllers.MainController;

public class MainClass extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(MainClass.class.getResource("views/Login.fxml"));

        //Setup stage with scene
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));

        //Set stage size to monitor width
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        MainController.PrimaryStage = primaryStage;

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
