package project.utility;

import javafx.scene.control.Alert;

/**
 * Created by Justin on 14-8-2016.
 */
public class General {
    public static void DisplayWarning(String Heading, String Message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(Heading);
        alert.setHeaderText(null);
        alert.setContentText(Message);

        alert.showAndWait();
    }


}
