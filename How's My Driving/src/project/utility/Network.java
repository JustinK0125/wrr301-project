package project.utility;

import project.controllers.MainController;
import sun.applet.Main;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by Justin on 14-8-2016.
 */
public class Network {
    public static void SendEmail(String[] Recipients, String Subject, String Body) throws Exception {

        /*

        Code supplied by "Bill The Lizard" from StackOverflow
        http://stackoverflow.com/questions/46663/how-can-i-send-an-email-by-java-application-using-gmail-yahoo-or-hotmail

        */

        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", MainController.EmailAddress);
        props.put("mail.smtp.password", MainController.Password);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(MainController.EmailAddress));
            InternetAddress[] toAddress = new InternetAddress[Recipients.length];

            // To get the array of addresses
            for( int i = 0; i < Recipients.length; i++ ) {
                toAddress[i] = new InternetAddress(Recipients[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(Subject);
            message.setText(Body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, MainController.EmailAddress, MainController.Password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (Exception e) {
            throw new Exception("Error sending email");
        }
    }
}
