package project.utility;

import org.w3c.dom.*;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Justin on 12-8-2016.
 */
public class XML {

    public static void saveDoc(Document doc, String filename) throws Exception {
        // obtain serializer
        DOMImplementation impl = doc.getImplementation();
        DOMImplementationLS implLS = (DOMImplementationLS) impl.getFeature("LS", "3.0");
        LSSerializer ser = implLS.createLSSerializer();
        ser.getDomConfig().setParameter("format-pretty-print", true);

        // create file to save too
        FileOutputStream fout = new FileOutputStream(filename);

        // set encoding options
        LSOutput lsOutput = implLS.createLSOutput();
        lsOutput.setEncoding("UTF-8");

        // tell to save xml output to file
        lsOutput.setByteStream(fout);

        // FINALLY write output
        ser.write(doc, lsOutput);

        // close file
        fout.close();
    }

    public static Element loadDoc() throws Exception{
        // create DocumentBuilder to parse XML document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        // load document and parse to create tree
        File fin = new File("Resources/Login.xml");
        Document doc = builder.parse(fin);

        // iterate through tree structure
        Element root = doc.getDocumentElement();
        return root;
    }

    public static void display(Element element, int depth) {
        // create a string of 'depth' tabs
        String indent = "";
        for (int i = 0; i < depth; i++)
            indent = indent.concat("\t");

        // get the name of the node
        String name = element.getNodeName();

        // display the beginning of the node
        System.out.printf("%sBEGIN(%s)\n", indent, name);

        NodeList nodes = element.getChildNodes();
        // for each node, display it (and children if any)
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            // is it a node containing child nodes?
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                // it is, so display the child at depth + 1
                Element childElement = (Element) node;
                display(childElement, depth + 1);

                // does the node contain text?
            } else if (node.getNodeType() == Node.TEXT_NODE) {
                // it does, so get the text and display it in quotes
                Text textNode = (Text) node;
                String data = textNode.getTextContent();
                // the string could contain spaces, new lines or tabs on either side of it, so trim these off
                data = data.trim();
                // anything left of the string? if so, display it
                if (data.length() > 0) System.out.printf("\t%s'%s'\n", indent, data);
            }
        }

        // display end of the node
        System.out.printf("%sEND(%s)\n", indent, name);
    }


}
