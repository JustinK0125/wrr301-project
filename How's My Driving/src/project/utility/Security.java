package project.utility;

import sun.plugin2.message.Message;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Random;

/**
 * Created by Justin on 13-8-2016.
 */
public class Security {

    public static String MD5HashString(String Input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(Input.getBytes(), 0, Input.length());
            return new BigInteger(1, md.digest()).toString(16);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String GenerateSalt() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        while (sb.length() < 8) {
            sb.append(Integer.toHexString(random.nextInt()));
        }
        return sb.toString();
    }
}
